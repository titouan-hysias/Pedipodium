# -*- coding: utf-8 -*-
import fichier_modif
import Sauvegarde
arbre_question = {}
arbre_numero = {8: {4: {2: {1: 0, 3: 0}, 6: {5: 0, 7: 0}}, 12: {10: {9: 0, 11: 0}, 14: {13: 0, 15: 0}}}}
arbre_filiaire = {1: {2: {3: {4: {5: {6: {7: {8: {9: {10: {11: {12: {13: {14: {15: 0}}}}}}}}}}}}}}}
chemin = []
relance = True
#print("A cause d'une erreuer d'encodage, les accents ne sont pas present.")

def correction(mot : dict.keys)->str:
    """dict.keys() donne : dict_keys([key,key...]) .Cette fonction permet d'avoir seulement : [key,key...]"""
    mot, ajout, liste = str(mot).translate(str.maketrans("", "", "dict_keys([])")), "", []
    for elem in mot:
        if elem != ",": ajout += elem
        else:
            liste.append(int(ajout))
            ajout = ""
    if len(ajout) > 0: liste.append(int(ajout))
    return liste
def recherche(arbre : dict, nb : int)->str:
    """Effectue une recherche dans un arbre binaire de recherche."""
    global chemin
    racine = correction(arbre.keys())[0]
    chemin.append(racine)
    if racine == nb: return str(racine)
    if arbre[racine] == 0: return False
    arbre2, clé2 = {}, correction(arbre[racine].keys())
    if nb < racine :
        arbre2[clé2[0]] = arbre[racine][clé2[0]]
        return str(racine) + "_" + recherche(arbre2, nb)
    else :
        arbre2[clé2[1]] = arbre[racine][clé2[1]]
        return str(racine) + "_" + recherche(arbre2, nb)


def validider_pseudo(pseudo : str)->str:
    if len(pseudo) < 4: return "Ce pseudo est trop court."
    if len(pseudo) > 8: return "Ce pseudo est trop long."
    caracteres_valide = "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    for lettre in pseudo:
        if lettre not in caracteres_valide: return f"Le caractère '{lettre}' n'est pas valide."
    M = fichier_modif.Modification("Sauvegarde.py")
    if M.trouver_ligne_pseudo(pseudo) != -1: return "Ce pseudo est déjà pris."
    return "Ce pseudo est bon."
def validider_mdp(mdp : str)->str:
    if len(mdp) < 4: return "Ce mot de passe est trop court."
    if len(mdp) > 8: return "Ce mot de passe est trop long."
    caracteres_valide = "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    for lettre in mdp:
        if lettre not in caracteres_valide: return f"Le caractère '{lettre}' n'est pas valide."
    return "Ce mot de passe est bon."

def reconnaissance_pseudo(pseudo : str)->str:
    if len(pseudo) < 4: return "Ce pseudo est trop court."
    if len(pseudo) > 8: return "Ce pseudo est trop long."
    caracteres_valide = "ABCDEFGHIJKLMONPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
    for lettre in pseudo:
        if lettre not in caracteres_valide: return f"Le caractère '{lettre}' n'est pas valide."
    if fichier_modif.Modification("Sauvegarde.py").trouver_ligne_pseudo(pseudo) != -1: return "Votre pseudo a bien été trouvé."
    return "Votre pseudo n'a pas été trouvé."
def reconnaissance_mdp(mdp : str)->str:
    if Sauvegarde.joueur[pseudo][0] == mdp: return "Bon retour."
    return "Ce n'est pas le bon mot de passe."

def identification()->None:
    global pseudo
    global mdp
    global arbre_numero
    global arbre_question
    global relance
    sortie = True
    while sortie:
        rep = input("Avez cous déjà un compte (oui/non) : ")
        if rep == "non":
            pseudo = input("Choisissez un pseudo : ")
            print(validider_pseudo(pseudo))
            if validider_pseudo(pseudo) == "Ce pseudo est bon.":
                mdp = input("Choisissez un mot de passe : ")
                print(validider_mdp(mdp))
                if validider_mdp(mdp) == "Ce mot de passe est bon.":
                    rep = input("Voulez-vous le mode difficile :")
                    if rep == "oui":
                        nb = [i for i in range(1, len(Sauvegarde.codes) + 1)]
                        import random
                        for loop in range(len(Sauvegarde.codes)):
                            index = random.choice(nb)
                            nb.remove(index)
                            arbre_question[Sauvegarde.codes[loop]] = (index, Sauvegarde.questions.index(Sauvegarde.questions[loop]), Sauvegarde.reponses[loop])
                    else :
                        for loop in range(len(Sauvegarde.codes)): arbre_question[Sauvegarde.codes[loop]] = (loop + 1 , Sauvegarde.questions.index(Sauvegarde.questions[loop]), Sauvegarde.reponses[loop])
                    M = fichier_modif.Modification("Sauvegarde.py")
                    #print(arbre_question)
                    M.ecrire_fin(f"\njoueur['{pseudo}'] = ['{mdp}', [], {arbre_question}]")
                    print("Votre compte a bien été crée,.")
                    R = fichier_modif.Recherche()
                    R.lancer("Jeu_question.py")
                    sortie, relance = False, False
        elif rep == "oui":
            pseudo = input("Quel est votre pseudo : ")
            print(reconnaissance_pseudo(pseudo))
            if reconnaissance_pseudo(pseudo) == "Votre pseudo a bien été trouvé.":
                mdp = input("Quel est votre mot de passe : ")
                print(reconnaissance_mdp(mdp))
                if reconnaissance_mdp(mdp) == "Bon retour.":
                    arbre_question = Sauvegarde.joueur[pseudo][2]
                    sortie = False

def sauvegarde()->None:
    M = fichier_modif.Modification("Sauvegarde.py")
    M.ecrire_fin(f"\njoueur['{pseudo}'] = ['{mdp}', {code_reussi}, {arbre_question}]") #Aller à la ligne ?

identification()
code_reussi = Sauvegarde.joueur[pseudo][1]

if relance : print("Pour jouer vous devez vous déplacer dans la ville du puy et trouver des codes.\nEnsuite vous rentrez un code et une question en latin apparait.\nVous devez utiliser le traducteur pour obtenir la question en francais puis y répondre.\nLes réponses doivent être en un seul mot (ou avec des tirets) ,une majuscule ou être des chiffres et sans accents.\nBon jeu.\n")
sortie = True
while sortie and relance:
    print("Les commandes possibles sont :\n    traducteur : ouvre le traducteur\n    sauvegarde : permet de sauvegarder\n    niveau : vous donne votre niveau\n    code : permet de tester un code\n    fermer : permet de fermer le jeu\n")
    rep = input("Que voulez vous faire : ")
    
    if rep == "traducteur":
        phrase = input("Quel est la phrase à traduire :")
        import secretum_v5
        print(evolution_phrase(phrase))
        
    if rep == "sauvegarde":
        sauvegarde()
        print("Votre fichier a bien été sauvegarder")
    
    if rep == "fermer": sortie = False
    
    if rep == "niveau": print(f"Vous êtes au niveau : {len(code_reussi)}/15\n")
    
    if rep == "code":
        rep = input("Quel est votre code : ")
        
        if rep in Sauvegarde.joueur[pseudo][1] : print("Ce code a déjà été valider.") #Vérifie les codes déjà rentrés
        
        elif rep in Sauvegarde.codes: #Vérifie si le code existe
            recherche(arbre_numero, arbre_question[rep][0]) #Modifie la variable chemin
            nombres = []
            for code in Sauvegarde.joueur[pseudo][1]:
                #code = "NSI" puis "JEU" ..
                nombres.append(Sauvegarde.joueur[pseudo][2][code][1])
                #Prend les numéros des question déjà répondu
            for nombre in nombres:
                if nombre not in chemin: chemin = [] #Vérifie si tout les numéros dans chemin sont dans les numéros des questions déjà répondu
            if chemin == []: print("Vous ne pouvvez pas encore répondre à cette question.\n")
            else :
                if input(f"{Sauvegarde.questions[arbre_question[rep][1]]}\nVotre réponse:") == arbre_question[rep][2]:
                    print("C'est la bonne réponse.\n")
                    code_reussi.append(rep)
                else : print("Mauvaise réponse.\n")
            chemin = []
        else : print("Ce code n'est pas bon.\n")
#if relance : sauvegarde()