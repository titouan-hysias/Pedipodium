# -*- coding: utf-8 -*-
joueur = {}
questions = ["cuando ' Simone Weil ' ést nata ?"
             ,"cumt béné ' Rocher ' Saint ' Michel computa dē levāRé dē gāmbas ?"
             ,"cumt béné aRtis distīncti ' Notre ' Dame ' du ' Puy  habēRé ?"
             ,"cualis camīnus paRtit dē ' Notre ' Dame ' du ' Puy ?"
             ,"uno mutto , cualis causa  ' vierge ' Puy cui ést millōcus ?"
             ,"dē cualis aRmas fundūtu ést fāctutum ' Notre ' Dame ' de ' France ?"
             ,"cualis īndustRia ' Ponote om tRopat hincoRa dēintus Rūga ' Des ' Tables ?"
             ,"Quid illum hospitālem 'Des Polignac' fuit sēdiculum dē 1800-1825 ?"
             ,"Illam causa qui distinguet illam mansionem 20 dēillam rūga 'Cardinal-De-Polignac' est quē illos fēnēstras sunt ... ?"
             ,"Om tropat quid in āngulus dē illam rūga 'Anne-Marie-Martel' ?"
             ,"Qu'est-ce qui orne le dessus des deux portes laterales de la chapelle du grand seminaire(Des ...) ?"
             ,"A qui la rue 'Des Cordeliers' doit t'elle son nom(Aux ...) ?"
             ,"Comment s'appelle les habitants du 'Puy-En-Velay'(Les...) ?"
             ,"Comment se nomme le fleuve qui traverse le 'Puy-En-Velay' (L'...) ?"
             ,"A quelle hauteur se situe le 'Puy-En-Velay' ?"]

while len(questions) < 15: questions.append(" ")
reponses = ["1909","268","3","Saint-Jacques-de-Compostelle","Noir","Canon","Dentelle","Prefecture","Murees","Viege","Anges","Cordiers","Ponots","Allier","744"]
codes = ["TITOUAN","HYSIAS","NSI","JEU","ORIENTATION","PYTHON","MARCHE","PUY-EN-VELAY","ORDINATEUR,","CODAGE","ERROR","LATIN","EMULATEUR","TROPHEES","INFORMATIQUE"]

joueur['Titouan'] = ['salut', [], {'TITOUAN': (1, 0, '1909'), 'HYSIAS': (2, 1, '268'), 'NSI': (3, 2, '3'), 'JEU': (4, 3, 'Saint-Jacques-de-Compostelle'), 'ORIENTATION': (5, 4, 'Noir'), 'PYTHON': (6, 5, 'Canon'), 'MARCHE': (7, 6, 'Dentelle'), 'PUY-EN-VELAY': (8, 7, 'Prefecture'), 'ORDINATEUR,': (9, 8, 'Murees'), 'CODAGE': (10, 9, 'Viege'), 'ERROR': (11, 10, 'Anges'), 'LATIN': (12, 11, 'Cordiers'), 'EMULATEUR': (13, 12, 'Ponots'), 'TROPHEES': (14, 13, 'Allier'), 'INFORMATIQUE': (15, 14, '744')}]