# -*- coding: utf-8 -*-

#TERMINER LA METHODE SCANSION !!!!! (1h) --> régler problème de scansion sur proparoxyton comme sperare
#TRADUIRE LES PHRASES ET LES TESTER (2h)
#FAIRE LES METHODES D'EXECUTION PUIS LE MODE TRADUCTION (1h30)
#faire des trucs pour l'orthographe.....(.....)

#==========================================================================
#              CLASSES DE MUTATION
#==========================================================================

class Voyelle :
    """Une classe pour créer les voyelles et y entrer les méthodes de mutation associées qui seront nécessaires pour la mutation des mots"""
    def __init__(self, aperture:int,ouverture:int,arrondissement:int,longueur:int,nasalisation:int,lettre:str):
        self.ap = aperture
        self.ou = ouverture
        self.ar = arrondissement
        self.lo = longueur
        self.na = nasalisation
        self.le = lettre
        self.tuple_ = (self.ap,self.ou,self.ar,self.lo,self.na)

    #méthodes générales
    def __repr__(self):
        return str(self.le)+str(self.tuple_)
    def affichage_ch(self):
        """affiche la lettre qui représente l'objet Voyelle"""
        print(str(self.le))
    def car2code(self, car:str)->tuple or str:
        """transforme chaque lettre chaine de caractere en tuple de code"""
        if car == self.le: return self.tuple_
        return car
    def code2car(self,elem:list)->str or list:
        """renvoi la lettre str à partir du tuple de code de l'objet Voyelle"""
        if elem[-1] == self.le: return self.le
        return elem

    #méthodes de mutation sur voyelle
    def M4(self):
        """Boul voc 1 : e_f > e_o"""
        for son in self.mot_obj:
            if son[1] == e_f: son[1] = e_o
    def M5(self):
        """Boul voc 1 : e_l > e_f"""
        for son in self.mot_obj:
            if son[1] == e_l : son[1] = e_f
    def M6(self):
        """Boul voc 1 : o_f > o_o"""
        for son in self.mot_obj:
            if son[1] == o_f : son[1] = o_o
    def M7(self):
        """Boul voc 1 : o_l > o_f"""
        for son in self.mot_obj:
            if son[1] == o_l : son[1] = o_f
    def M8(self):
        """Boul voc 1 : a_l > a_"""
        for son in self.mot_obj:
            if son[1] == a_l : son[1] = a_
    def M11(self):
        """Boul voc : i_ > e_f"""
        for son in self.mot_obj:
            if son[1] == i_: son[1] = e_f
    def M12(self):
        """Boul voc : i_l > i_"""
        for son in self.mot_obj:
            if son[1] == i_l: son[1] = i_
    def M17(self):
        """e_o initial > e_f"""
        for son in self.mot_obj:
            if son == [1,e_o]: son[1]=e_f
    def M19(self):
        """a_/a_l > e_ en prétonique interne"""
        for son in self.mot_obj:
            if son == [2,a_] or son == [2,a_l]: son[1] = e_
    def M23(self):
        """u_ final > o_f"""
        if self.mot_obj[-1][1] == u_: self.mot_obj[-1][1] = o_f
    def M26(self):
        """a_ final > e_"""
        for son in self.mot_obj:
            if son == [5,a_]: son[1] = e_
    def M29(self):
        """o_o initial > o_f"""
        for son in self.mot_obj:
            if son == [1,o_o]: son[1] = o_f
    def M91(self):
        """supression de e_ final derrière voyelle"""
        if self.mot_obj[-1][1] == e_ and self.mot_obj[-2][1] in isvoyelle:
            self.mot_obj.remove(self.mot_obj[-1])
    def M93(self):
        """e_ final > oe_"""
        if self.mot_obj[-1][1] == e_ :self.mot_obj[-1][1] = oe_
    def M97(self):
        """oe_ final disparait"""
        if self.mot_obj[-1][1] == oe_: self.mot_obj.remove(self.mot_obj[-1])
    def M100(self):
        """i_e_f > y_e_f"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == e_f and self.mot_obj[i-1][1] == i_ and i != 0: self.mot_obj[i-1][1] = y_

#objets Voyelle
a_ = Voyelle(1,5,0,0,0,'a')
a_l = Voyelle(1,5,0,1,0,'ā')
e_o = Voyelle(1,4,0,0,0,'è')
oe_ = Voyelle(1,4,1,0,0,'œ')
e_n = Voyelle(1,4,0,0,1,'ẽ')
oe_n = Voyelle(1,4,1,0,1,'œ̃')
e_ = Voyelle(2,3,0,0,0,'e')
e_f = Voyelle(1,2,0,0,0,'é')
e_l = Voyelle(1,2,0,1,0,'ē')
eu_ = Voyelle(1,2,1,0,0,'ë')
i_ = Voyelle(1,1,0,0,0,'i')
i_l = Voyelle(1,1,0,1,0,'ī')
u_v = Voyelle(1,1,1,0,0,'ü')
u_ = Voyelle(3,1,1,0,0,'u')
u_l = Voyelle(3,1,1,1,0,'ū')
o_f = Voyelle(3,2,1,0,0,'o')
o_l = Voyelle(3,2,1,1,0,'ō')
o_o = Voyelle(3,4,1,0,0,'ô')
o_n = Voyelle(3,4,1,0,1,'õ')
a_v = Voyelle(3,5,0,0,0,'â')
a_n = Voyelle(3,5,0,0,1,'ã')

isvoyelle=[a_,a_l,e_o,oe_,e_n,oe_n,e_,e_f,e_l,eu_,i_,i_l,u_v,u_,u_l,o_f,o_l,o_o,o_n,a_v,a_n]
isvoyelle_courte=[a_,e_o,e_f,oe_,oe_n,e_n,e_,e_f,eu_,i_,u_v,u_,o_f,o_o,o_n,a_v,a_n]
isvoyelle_longue=[a_l,i_l,e_l,u_l,o_l]
isvoyelle_longue_ch=["ā",'ē','ī','ū','ō']
isvoyelle_courte_ch = ["a","è","œ",'ẽ','œ̃',"e","é","ë","i","ü","u","o","ô",'õ','â','ã']
isvoyelle_ch = isvoyelle_longue_ch + isvoyelle_courte_ch

class Consonne :
    """Une classe pour créer les consonnes y entrer les méthodes de mutation associées qui seront nécessaires pour la mutation des mots"""
    def __init__(self, position:int,point_articulation:int,voisement:int,lettre:str):
        self.po = position
        self.pa = point_articulation
        self.vo = voisement
        self.le = lettre
        self.tuple_ = (self.po,self.pa,self.vo)

    #méthodes générales
    def __repr__(self):
        return str(self.le)+str(self.tuple_)
    def affichage_ch(self):
        """affiche la lettre qui représente l'objet Voyelle"""
        print(str(self.le))
    def car2code(self, car:str)->tuple or str:
        """transforme chaque lettre chaine de caractere en tuple de code"""
        if car == self.le: return self.tuple_
        return car
    def code2car(self,elem:list)->str or list:
        """renvoi la lettre str à partir du tuple de code de l'objet Voyelle"""
        if elem[-1] == self.le: return self.le
        return elem

    #méthodes de mutation sur consonnes
    def M1(self):
        """suppression du m_ en position finale"""
        if self.mot_obj[-1][1] == m_: self.mot_obj = self.mot_obj[0:-1]
    def M9(self):
        """w_ > v_"""
        for son in self.mot_obj:
            if son[1] == w_ : son[1] = v_
    def M49(self):
        """chute de s_ final devant u_"""
        if self.mot_obj[-1][1] == s_ and self.mot_obj[-2][1] == u_:
            self.mot_obj = self.mot_obj[0:-1]
    def M77(self):
        """z_ final > s_"""
        if self.mot_obj[-1][1] == z_: self.mot_obj[-1][1] = s_
    def M81(self):
        """ts_ > s_"""
        for son in self.mot_obj:
            if son[1] == ts_: son[1] = s_
    def M82(self):
        """dz_ > z_"""
        for son in self.mot_obj:
            if son[1] == dz_: son[1] = z_
    def M83(self):
        """tch_ > ch_"""
        for son in self.mot_obj:
            if son[1] == tch_: son[1] = ch_
    def M84(self):
        """dj_ > j_"""
        for son in self.mot_obj:
            if son[1] == dj_: son[1] = j_
    def M92(self):
        """supression du s_ final atone"""
        if self.mot_obj[-1] == [5,s_]: self.mot_obj.remove(self.mot_obj[-1])
    def M95(self):
        """supression du t_ final atone derrière consonne"""
        if self.mot_obj[-1] == [5,t_] and self.mot_obj[-2][1] in isconsonne: self.mot_obj.remove(self.mot_obj[-1])
    def M96(self):
        """chute de l_v en final"""
        if self.mot_obj[-1][1] == l_v: self.mot_obj.remove(self.mot_obj[-1])
    def M102(self):
        """ r roulé r_ se vélarise r_ > r_v"""
        for son in self.mot_obj:
            if son[1] == r_: son[1] = r_v
    def M108(self):
        """perte du h_ latin en initial"""
        if self.mot_obj[0][1] == h_ : self.mot_obj.remove(self.mot_obj[0])

#objets Consonnes
m_ = Consonne(1,1,1,'m')
b_ = Consonne(1,2,1,'b')
p_ = Consonne(1,2,0,'p')
f_ = Consonne(2,3,0,'f')
v_ = Consonne(2,3,1,'v')
w_ = Consonne(1,4,1,'w')
th_ = Consonne(3,3,0,'th')
dh_ = Consonne(3,3,1,'dh')
n_ = Consonne(4,1,1,'n')
d_ = Consonne(4,2,1,'d')
t_ = Consonne(4,2,0,'t')
s_ = Consonne(4,3,0,'s')
z_ = Consonne(4,3,1,'z')
r_ = Consonne(4,5,1,'R')
l_ = Consonne(4,6,1,'l')
ts_ = Consonne(4,7,0,'ts')
dz_ = Consonne(4,7,1,'dz')
ch_ = Consonne(5,3,0,'ch')
j_ = Consonne(5,3,1,'j')
tch_ = Consonne(5,7,0,'tch')
dj_ = Consonne(5,7,1,'dj')
gn_ = Consonne(6,1,1,'ñ')
y_ = Consonne(6,4,1,'y')
ng_ = Consonne(7,1,1,'ng')
c_ = Consonne(7,2,0,'c')
g_ = Consonne(7,2,1,'g')
r_v = Consonne(8,3,1,'r')
h_ = Consonne(9,3,1,'h')
l_v = Consonne(6,6,1,'ɫ')
b_f = Consonne(1,3,1,'β')
g_f = Consonne(7,3,1,'γ')

isconsonne=[m_,b_,b_f,p_,f_,v_,w_,th_,dh_,n_,t_,d_,s_,z_,r_,l_,l_v,ts_,dz_,ch_,j_,tch_,dj_,gn_,y_,ng_,c_,g_,g_f,r_v,h_]
isvoycons_2 = [a_,a_l,e_o,oe_,e_n,oe_n,e_,e_f,e_l,eu_,i_,i_l,u_v,u_,u_l,o_f,o_l,o_o,o_n,a_v,a_n,m_,b_,p_,f_,v_,w_,th_,dh_,n_,t_,s_,z_,r_,l_,ts_,dz_,ch_,j_,tch_,dj_,gn_,y_,ng_,c_,g_,r_v,h_]
isvoycons = isvoyelle + isconsonne
isconsonne_ch=["m","b","p","f","v","w","th","dh","n","d","t","s","z","R","l","ts","dz","ch","j","tch","dj","ñ","y","gn","c","g","r","ɫ","β","γ","h"]

class Mot(Voyelle,Consonne):
    """Une classe fille de Syllabe qui permet de manipuler les mots et de permettre leurs mutations par les méthodes définies"""
    #méthodes utilisées dans le constructeur :
    def div_syl(self):
        """découpe le mot sous chaine de caractere en une liste de chaque syllabe"""
        lst_syl = list()
        current_syl = ""
        for car in self.mot_ch:
            if car in isvoyelle_ch:
                current_syl += car
                lst_syl.append(current_syl)
                current_syl = ""
            else:
                current_syl += car
        if current_syl:
            lst_syl.append(current_syl)
        if lst_syl[-1] in isconsonne_ch:
            lst_syl[-2] += lst_syl[-1]
            lst_syl.remove(lst_syl[-1])
        for i,syl in enumerate(lst_syl):
            for lettre in syl:
                if lettre in isvoyelle_ch and i != len(lst_syl)-1 and lst_syl[i+1] in isvoyelle_ch:
                    lst_syl[i] += lst_syl[i+1]
                    lst_syl.remove(lst_syl[i+1])
        return lst_syl
    #terminer la méthode scansion
    def scansion(self)->list:
        """méthode permet de scander les mots : il prédit pour chaque element tuple sa position
        et l'insère dans la liste syllabe et renvoi la liste de toutes les positions de chaque lettre.
        C'est une méthode qui étudie chaque possibilité de forme du mot car c'est le seul moyen trouvé
        et le plus simple afin de scander le mot (nous n'avons pas pu trouver un cas général)"""
        lst_position = list()
        lst_syl = self.div_syl()
        if len(lst_syl) == 1:
            for _ in range(len(lst_syl[0])):lst_position.append(3)
        if len(lst_syl) == 2:
            for _ in range(len(lst_syl[0])):lst_position.append(3)
            for _ in range(len(lst_syl[1])):lst_position.append(5)
        for position,syl in enumerate(lst_syl):
            for i,car in enumerate(syl):
                if len(lst_syl) == 3:
                    if car in isvoyelle_longue_ch and position==1 and len(lst_position) == 0:
                        for _ in range(len(lst_syl[0])):lst_position.append(1)
                        for _ in range(len(lst_syl[1])):lst_position.append(3)
                        for _ in range(len(lst_syl[2])):lst_position.append(5)
                    if car in isvoyelle_longue_ch and position==0 and len(lst_position) == 0:
                        for car_2 in syl:
                            if car_2 in isvoyelle_longue_ch and car_2 != car:
                                for _ in range(len(lst_syl[0])):lst_position.append(1)
                                for _ in range(len(lst_syl[1])):lst_position.append(3)
                                for _ in range(len(lst_syl[2])):lst_position.append(5)
                            else:
                                for _ in range(len(lst_syl[0])):lst_position.append(3)
                                for _ in range(len(lst_syl[1])):lst_position.append(4)
                                for _ in range(len(lst_syl[2])):lst_position.append(5)
                    elif len(lst_position) == 0 and car not in isvoyelle_longue_ch and i==len(syl)-1 and position==2:
                        for _ in range(len(lst_syl[0])):lst_position.append(3)
                        for _ in range(len(lst_syl[1])):lst_position.append(4)
                        for _ in range(len(lst_syl[2])):lst_position.append(5)
                    if car=="l" and syl[i-1]=="l" and len(lst_position)==0:#tentative de réglage du bug de scannsion sur les proparoxyton avec cas particulier...
                        for _ in range(len(lst_syl[0])):lst_position.append(1)
                        for _ in range(len(lst_syl[1])):lst_position.append(3)
                        for _ in range(len(lst_syl[2])):lst_position.append(5)
                if len(lst_syl) == 4:
                    if car in isvoyelle_longue_ch and position==1:
                        for _ in range(len(lst_syl[0])):lst_position.append(1)
                        for _ in range(len(lst_syl[1])):lst_position.append(3)
                        for _ in range(len(lst_syl[2])):lst_position.append(4)
                        for _ in range(len(lst_syl[3])):lst_position.append(5)
                    if car in isvoyelle_longue_ch and position==2:
                        for _ in range(len(lst_syl[0])):lst_position.append(1)
                        for _ in range(len(lst_syl[1])):lst_position.append(2)
                        for _ in range(len(lst_syl[2])):lst_position.append(3)
                        for _ in range(len(lst_syl[3])):lst_position.append(5)
                if len(lst_syl) == 5:
                    if car in isvoyelle_longue_ch and position == 2:
                        for _ in range(len(lst_syl[0])):lst_position.append(1)
                        for _ in range(len(lst_syl[1])):lst_position.append(2)
                        for _ in range(len(lst_syl[2])):lst_position.append(3)
                        for _ in range(len(lst_syl[3])):lst_position.append(4)
                        for _ in range(len(lst_syl[4])):lst_position.append(5)
        return lst_position
    def matrice_objet(self)->list:#méthode générale déplacée car appelée une fois pour permettre au constructeur de créer l'attribut mat_obj
        """transforme le mot sous chaine de caractere en la matrice composée de chaque objet son et de sa position dans une liste"""
        mat_mot=[[]*len(self.mot_ch) for _ in range(len(self.mot_ch))]
        for i in range(len(self.mot_ch)):
            for obj_son in isvoycons:
                if self.mot_ch[i]==obj_son.le:
                    mat_mot[i].append(self.lst_position[i])
                    mat_mot[i].append(obj_son)
        return mat_mot
    #constructeur
    def __init__(self,mot_chainecar,liste_position=None):
        self.mot_ch = mot_chainecar
        self.lst_position = self.scansion()
        self.mot_obj = self.matrice_objet()#type list

    #méthodes générales

    def construction(self)->list:
        """transforme le mot sous matrice_objet en la matrice de code du mot composée d'un indice de position et d'un objet son"""
        mat_mot=[[]*len(self.mot_obj) for _ in range(len(self.mot_obj))]
        for i in range(len(self.mot_obj)):
            mat_mot[i].append(self.mot_obj[i][0])
            mat_mot[i].append(self.mot_obj[i][1].tuple_)
        return mat_mot
    def transliterration(self):
        """transforme le mot sous forme de matrice d'objet en chaine de caractere"""
        mot_ch = str()
        for son in self.mot_obj:
            mot_ch += son[1].le
        return mot_ch
    def __str__(self):
        self.mot_ch = self.transliterration()
        return str(self.mot_obj)+" , "+str(self.mot_ch)

    #méthodes mutations
    def M2(self):
        """effacement de s_ devant n_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == s_ and self.mot_obj[i-1][1] == n_ and self.mot_obj[i-2][0]==3:
                self.mot_obj[i-1:i+1] = [son]
    def M3(self):
        """développement du i_ prothétique en début de mot"""
        if self.mot_obj[0][1] == s_ and self.mot_obj[1][1] in isconsonne:
            self.mot_obj.insert(0,[1,i_])
            if self.mot_obj[3][1] in isvoyelle_courte:
                self.mot_obj[3][0] = 2
    def M10(self):
        """b_ intervocalique > v_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == b_ and i != 0 and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+1][1] in isvoyelle:
                son[1] = v_
    def M13(self):
        """fausse palatalisation de : g_ intervocalique + i_/e_f > y_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == g_ and i != 0 and self.mot_obj[i+1][1] in (i_,e_f,e_o) and self.mot_obj[i-1][1] in isvoyelle:
                son[1] = y_
    def M14(self):
        """p_+liquide et intervocalique > b_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == p_ and i != 0 and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+1][1] in (l_,l_v,r_):
                son[1] = b_
    def M15(self):
        """diphtonguaison e_o tonique libre > i_e_f"""
        for i, son in enumerate(self.mot_obj):
            if son == [3,e_o] and i != 0 and i != len(self.mot_obj)-2 and self.mot_obj[i+2][1] in isvoyelle and self.mot_obj[i-1][1] in isconsonne:
                self.mot_obj[i:i+1] = [[3,i_],[3,e_f]]
    def M16(self):
        """affaiblissement prétonique interne libre"""
        for i,son in enumerate(self.mot_obj):
            if son[0] == 2 and son[1] in isvoyelle and son[1] != e_ and self.mot_obj[i+1][1] in isconsonne and self.mot_obj[i+2][1] in isvoyelle:
                self.mot_obj.remove(son)
    def M18(self):
        """s_ intervocalique > z_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == s_ and i != 0 and i != len(self.mot_obj)-1 and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+1][1] in isvoyelle:
                son[1] = z_
    def M20(self):
        """amuisement de la voyelle courte en pénultième libre"""
        for i,son in enumerate(self.mot_obj):
            if son[0] == 4 and son[1] in (a_,e_f,e_o,i_,o_o,o_f,u_):
                self.mot_obj.remove(son)
    def M21(self):
        """c_/g_ implosif devant consonne > y_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (c_,g_) and self.mot_obj[i+1][1] in isconsonne and self.mot_obj[i-1][0] == 3:
                son[1] = y_
    def M22(self):
        """u_ intérieur non-libre > o_f"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == u_ and self.mot_obj[i-1][1] in isconsonne and i != 0 and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isconsonne:
                son[1] = o_f
    def M24(self):
        """t_ intervocalique + liquide > d_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (l_,l_v,r_) and i != 0 and self.mot_obj[i-1][1] == t_ and self.mot_obj[i-2][1] in isvoyelle:
                self.mot_obj[i-1][1] = d_
    def M25(self):
        """t_ intervocalique > d_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in isvoyelle and i != 0 and self.mot_obj[i-1][1] == t_ and self.mot_obj[i-2][1] in isvoyelle:
                self.mot_obj[i-1][1] = d_
    def M27(self):
        """l_ implosif avant consonne > l_v"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == l_ and self.mot_obj[i-2][0] == 3 and self.mot_obj[i-1][1] in isconsonne:
                son[1] = l_v
    def M28(self):
        """fermeture et consonification du e_f et i_ atone en hiatus > s_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (i_,e_f) and i != 0 and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isvoyelle:
                son[1] = s_
    def M30(self):
        """palatalisation de c_ + i_/e_f/e_o > ts_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == c_ and self.mot_obj[i+1][1] in (i_,e_f,e_o):
                son[1] = ts_
    def M31(self):
        """palatalisation de g_ + i_/e_f/e_o > dj"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == g_ and self.mot_obj[i+1][1] in (i_,e_f,e_o):
                son[1] = dj_
    def M32(self):
        """c_ intervocalique > g_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == c_ and self.mot_obj[i+1][1] in isvoyelle and self.mot_obj[i-1][1] in isvoyelle and i != 0:
                son[1] = g_
    def M33(self):
        """l_v > u diphtongue"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == l_v and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i-1][1] != u_:
                son[1] = u_
    def M34(self):
        """diphtongaison o_o tonique > u_o_f"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,o_o] and i < len(self.mot_obj)-2 and self.mot_obj[i+1][1] in isconsonne and self.mot_obj[i+2][1] in isvoyelle:
                self.mot_obj[i:i+1] = [[3,u_],[3,o_f]]
    def M35(self):
        """affaiblissement de la voyelle finale proparoxytons en e_"""
        for i,son in enumerate(self.mot_obj):
            if son[0] == 4 and self.mot_obj[3][0] != 2 and self.mot_obj[-1][1] in isvoyelle:
                self.mot_obj[-1][1] = e_
    def M35_bis(self):
        """affaiblissement de la séquence finale proparoxytons en e_ si consonne/voyelle finale"""
        for i,son in enumerate(self.mot_obj):
            if son[0] == 4 and self.mot_obj[3][0] != 2 and self.mot_obj[-1][1] in isconsonne and self.mot_obj[-2][1] in isvoyelle:
                self.mot_obj[-2][1] = e_
                self.mot_obj.remove(self.mot_obj[-1])
    def M36(self):
        """spirantisation b_ intervocalique > b_f"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == b_ and self.mot_obj[i-1][1] in isvoyelle and i != 0 and self.mot_obj[i+1][1] in isvoyelle:
                son[1] = b_f
    def M37(self):
        """spirantisation g_ intervocalique > g_f"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == g_ and self.mot_obj[i-1][1] in isvoyelle and i != 0 and self.mot_obj[i+1][1] in isvoyelle:
                son[1] = g_f
    def M38(self):
        """palatalisation avec effet de Bartsch: c_ + a_ non-libre > tch_ + e_o"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == c_ and i < len(self.mot_obj)-1 and self.mot_obj[i+1][1] == a_ and self.mot_obj[i+2][1] in isconsonne and self.mot_obj[i+3][1] in isconsonne:
                son[1] = tch_
                self.mot_obj[i+1:i+2] = [[son[0],i_],[son[0],e_f]]
    def M39(self):
        """palatalisation avec effet_de Bartsch: c_ + a_ libre > tch_ + i_e_f"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == c_ and self.mot_obj[i+1][1] == a_ and self.mot_obj[i+2][1] in isconsonne and self.mot_obj[i+3][1] in isvoyelle:
                son[1] = tch_
                self.mot_obj[i+1:i+2] = [[son[0],i_],[son[0],e_f]]
    def M40(self):
        """palatalisation avec effet de Bartsch: g_ + a_ > dj_ + i_e_f"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == g_ and self.mot_obj[i+1][1] == a_ :
                son[1] = dj_
                self.mot_obj[i+1][1] = e_o
    def M41(self):
        """b_ intervocalique + liquide > v_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == b_ and i != 0 and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+1][1] in (l_,l_v,r_):
                son[1] = v_
    def M42(self):
        """monophtongaison primaire de a_u_ > o_o et palatalisation de c_+a_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == c_ and self.mot_obj[i+1][1] == a_ and self.mot_obj[i+2][1] == u_:
                son[1] = tch_
                self.mot_obj[i+1:i+3] = [[son[0],o_o]]
    def M43(self):
        """fausse palatalisation de g_ + a_ secondaire dans un environnement palatal : g_ + a_ > g_f + a_ > y_ + a_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == g_f and self.mot_obj[i+1][1] == a_:
                self.mot_obj[i:i+1] = [[son[0],y_]]
    def M44(self):
        """diph francienne : e_f tonique libre > e_f i_"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,e_f] and i > 1 and i != len(self.mot_obj)-2 and self.mot_obj[i-1][1] in isconsonne and self.mot_obj[i+1][1] in isconsonne and (self.mot_obj[i+2][1] in isvoyelle or self.mot_obj[i+2][1] in (r_,l_,l_v)):
                self.mot_obj.insert(i+1,[3,i_])
    def M45(self):
        """diph francienne: o_f tonique libre > o_f u_"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,o_f] and len(self.mot_obj)-2 > 2 and self.mot_obj[i-1][1] in isconsonne and self.mot_obj[i+1][1] in isconsonne and (self.mot_obj[i+2][1] in isvoyelle or self.mot_obj[i+2][1] in (r_,l_,l_v)):
                self.mot_obj.insert(i+1,[3,u_])
    def M46(self):
        """diph francienne: a_ tonique libre > a_e_f"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,a_] and i != len(self.mot_obj)-2 and self.mot_obj[i+1][1] in isconsonne and (self.mot_obj[i+2][1] in isvoyelle or self.mot_obj[i+2][1] == r_):
                self.mot_obj.insert(i+1,[3,e_f])
    def M47(self):
        """spirantisation de d_ intervocalique > dh_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == d_ and i != 0 and i!=len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isvoyelle and self.mot_obj[i-1][1] in isvoyelle:
                son[1] = dh_
    def M47_bis(self):
        """spirantisation de d_ + liquide intervocalique > dh_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == d_ and i != 0 and i!=len(self.mot_obj)-1 and self.mot_obj[i+1][1] in (l_,r_,l_v) and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+2][1] in isvoyelle:
                son[1] = dh_
    def M48(self):
        """fermeture partielle de i_e_f non-libre > e_o"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == i_ and self.mot_obj[i+1][1] == e_f and self.mot_obj[i+2][1] in isconsonne and self.mot_obj[i+3][1] in isconsonne:
                self.mot_obj[i:i+2] = [[son[0],e_o]]
    def M50(self):
        """amuisement des voyelles finales derrière s_/t_"""
        if self.mot_obj[-1][1] in isconsonne and self.mot_obj[-2][1] in isvoyelle:
            self.mot_obj.remove(self.mot_obj[-2])
    def M51(self):
        """palatalisation de c_ + a_ > tch_ si consonne non-libre ou final"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,c_] and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] == a_ and ((self.mot_obj[i+1] == self.mot_obj[len(self.mot_obj)-1]) or ((self.mot_obj[i+1] == self.mot_obj[len(self.mot_obj)-2]) and (self.mot_obj[-1][1] in isconsonne))):
                son[1] = tch_
            elif son[1] == c_ and i!=len(self.mot_obj)-2 and self.mot_obj[i+1][1] == a_ and self.mot_obj[i+2][1] in isconsonne and self.mot_obj[i+3][1] in isconsonne and self.mot_obj[i+3][1] not in (l_,l_v,r_):
                son[1] = tch_
    def M52(self):
        """affaiblissement de palatalisation tertiaire de g_f intervocalique + u_ + voy > w_ avec assimilation du u_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == g_f and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+1][1] == u_ and self.mot_obj[i+2][1] in isvoyelle:
                self.mot_obj[i:i+2] = [[son[0],w_]]
    def M53(self):
        """affaiblissement et effacement de certaines implosives par assimilation regressive: t_/d_ + s_ > s_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (d_,t_) and i != len(self.mot_obj)-1 and self.mot_obj[i+1] == [3,s_] and self.mot_obj[i+2][1] in isvoyelle:
                son[1] = s_
    def M54(self):
        """simpliffication géminées intervocaliques: t_t_ > t_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == t_ and self.mot_obj[i-1][1] == t_:
                self.mot_obj.remove(son)
    def M55(self):
        """simplification géminées intervocaliques: s_s_ > s_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == s_ and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] == s_:
                self.mot_obj.remove(son)
    def M56(self):
        """monopthongaison a_e_f tonique > e_f et déclenchement suppression du r_ infinitifs premier groupe"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,a_] and self.mot_obj[i+1] == [3,e_f]:
                self.mot_obj[i:i+2] = [[son[0],e_f]]
                if self.mot_obj[-1][1] == r_ and self.mot_obj[-2][1] == e_f:
                    self.mot_obj.remove(self.mot_obj[-1])
    def M57(self):
        """vocalisation y_ transition derrière consonne devant voyelle > i_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == y_ and self.mot_obj[i-1][1] in isvoyelle and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in (p_,b_,v_,f_,t_,d_,s_,z_,ch_,j_,tch_,dj_,ts_,dz_,gn_,y_,c_,g_,r_):
                son[1] = i_
    def M57_bis(self):
        """simplification de la séquence i_y_/ü_y > i_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (i_,u_v) and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] == y_:
                self.mot_obj.remove(self.mot_obj[i+1])
    def M57_tris(self):
        """simplification de la séquence i_i_ > i_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == i_ and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] == i_:
                self.mot_obj.remove(self.mot_obj[i+1])
    def M58(self):
        """affaiblissement l_/l_v implosif > w_ derrière consonne"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (l_,l_v) and self.mot_obj[i-1][1] in isvoyelle and (self.mot_obj[i-1][1] != e_f or len(self.mot_obj) > 4) and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isconsonne:
                son[1] = w_
    def M59(self):
        """palatalisation u_/u_l non libre > ü"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (u_,u_l) and self.mot_obj[i+1][1] in isconsonne:
                son[1] = u_v
    def M60(self):
        """disparition voyelle finales latines conservées"""
        if self.mot_obj[-1][1] in (o_o,o_f,u_,u_l,a_,e_f,e_o) and len(self.mot_obj) > 2 and self.mot_obj[-2][1] in isconsonne:
            self.mot_obj.remove(self.mot_obj[-1])
    def M61(self):
        """amuisement de dh_intervocalique ou en dh_ intervocalique + liquide"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == dh_ and i != len(self.mot_obj)-1 and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+1][1] in isvoyelle:
                self.mot_obj.remove(son)
            elif son[1] == dh_ and i != len(self.mot_obj)-1 and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+1][1] in (l_,l_v,r_):
                self.mot_obj.remove(son)
    def M62(self):
        """amuisement du t_/th_ final derrière e_"""
        if self.mot_obj[-1][1] in (t_,th_) and self.mot_obj[-2][1] == e_ :
            self.mot_obj.remove(self.mot_obj[-1][1])
    def M63(self):
        """e_f initial libre > e_"""
        for i,son in enumerate(self.mot_obj):
            if son == [1,e_f] and i != 0 and self.mot_obj[i-1][1] in isconsonne and self.mot_obj[i+1][1] in isconsonne and self.mot_obj[0][1] in isconsonne:
                son[1] = e_
    def M64(self):
        """amuisement du t_/th_ final derrière voyelle tonique"""
        if self.mot_obj[-1][1] in (t_,th_) and self.mot_obj[-2][1] in isvoyelle and self.mot_obj[-2][0] == 3:
            self.mot_obj.remove(self.mot_obj[-1])
    def M64_bis(self):
        """amuisement du t_/th_ + s_ final derrière voyelle tonique"""
        if self.mot_obj[-1][1] == s_ and self.mot_obj[-3][1] in isvoyelle and self.mot_obj[-3][0] == 3 and self.mot_obj[-2][1] in (t_,th_):
            self.mot_obj.remove(self.mot_obj[-1])
    def M65(self):
        """amuisement du d_/dh_ final derrière voyelle tonique"""
        if len(self.mot_obj) > 2 and self.mot_obj[-1][1] in (d_,dh_) and self.mot_obj[-2][1] in isvoyelle and self.mot_obj[-2][0] == 3:
            self.mot_obj.remove(self.mot_obj[-1])
    def M66(self):
        """sonorisation de s_ > z_ derrière consonne sonore"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == s_ and i != 0 and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i+1][1] in (m_,b_,w_,n_,d_,z_,l_,r_,l_v,y_,gn_,g_,g_f):
                son[1] = z_
    def M67(self):
        """effacement du z_ implosif derrière consonne"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == z_ and i != 0 and i != len(self.mot_obj)-1 and self.mot_obj[i-1][1] in isvoyelle and self.mot_obj[i-1][0]==3 and self.mot_obj[i+1][1] in (m_,b_,w_,n_,d_,z_,l_,r_,l_v,y_,gn_,g_,g_f):
                self.mot_obj.remove(son)
    def M68(self):
        """différenciation de u_o_f > u_e_f ..."""
        for i,son in enumerate(self.mot_obj):
            if son[1] == o_f and self.mot_obj[i-1][1] == u_:
                son[1] = e_f
    def M69(self):
        """nasalisation du a_ > a_n"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == a_  and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in (n_,m_):
                son[1] = a_n
    def M70(self):
        """nasalisation du e_f > e_n"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == e_f  and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in (n_,m_):
                son[1] = e_n
    def M71(self):
        """réduction de la diphtongue de coalescence a_i_ tonique > e_o"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,a_]  and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] == i_:
                self.mot_obj[i:i+2] = [[son[0],e_o]]
    def M72(self):
        """fermeture voyelle initiale en hiatus devant tonique: voyelle > e_"""
        for i,son in enumerate(self.mot_obj):
            if son[0] == 1 and son[1] in isvoyelle and self.mot_obj[i+1][1] in isvoyelle and self.mot_obj[i+1][0] == 3:
                son[1] = e_
    def M73(self):
        """simplification dh_ + s_ > s_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == s_ and self.mot_obj[i-1][1] == dh_:
                self.mot_obj.remove(self.mot_obj[i-1])
    def M74(self):
        """simplification b en 4 derrière voyelle tonique devant consonne occlusive"""
        for i,son in enumerate(self.mot_obj):
            if son == [4,b_] and self.mot_obj[i+1][1] in (p_,b_,t_,d_,c_,g_) and self.mot_obj[i-1][0]==3 and self.mot_obj[i-1][1] in isvoyelle:
                self.mot_obj.remove(son)
    def M75(self):
        """fermeture o_f initial en u_"""
        for i,son in enumerate(self.mot_obj):
            if son == [1,o_f] and self.mot_obj[i+1][1] in isconsonne and self.mot_obj[i-1][1] in isconsonne and i != 0:
                son[1] = u_
            elif self.mot_obj[0][0] == 3 and son == [3,o_f] and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isconsonne and self.mot_obj[i-1][1] in isconsonne:
                son[1] = u_
    def M76(self):
        """effacement du s implosif devant consonne, en fin de syllabe"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == s_ and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isconsonne and self.mot_obj[i-1][1] in isvoyelle and (son[0] != self.mot_obj[i-1][0] or son[0] != self.mot_obj[i+1][0]):
                self.mot_obj.remove(son)
    def M78(self):
        """e_f i_ tonique > o_f i_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == i_ and self.mot_obj[i-1] == [3,e_f]:
                self.mot_obj[i-1][1] = o_f
                son[0] = 3

    def M79(self):
        """o_f u_ tonique > e_f u_"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,o_f] and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] == u_:
                son[1] = e_f
    def M80(self):
        """w_ implosif devant consonne se vocalise > u_ (fin de M58)"""
        for i,son in enumerate(self.mot_obj):
            if son[1] ==w_ and self.mot_obj[i-1][1] in isvoyelle and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isconsonne:
                son[1] = u_
    def M85(self):
        """o_f i_ tonique > o_f e_f"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,i_] and self.mot_obj[i-1][1] == o_f: son[1] = e_f
    def M86(self):
        """o_f e_f tonique > u_e_f"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,e_f] and self.mot_obj[i-1][1] == o_f: self.mot_obj[i-1][1] = u_
    def M87(self):
        """u_e_f tonique > w_e_f"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,e_f] and self.mot_obj[i-1][1] == u_: self.mot_obj[i-1][1] = w_
    def M88(self):
        """w_e_f tonique > w_e_o"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,e_f] and self.mot_obj[i-1][1] == w_: son[1] = e_o
    def M89(self):
        """w_e_o tonique > e_o derrière consonne+liquide"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,e_o] and self.mot_obj[i-1][1] == w_ and self.mot_obj[i-2][1] in (r_,l_,l_v) and self.mot_obj[i-3][1] in isconsonne:
                self.mot_obj.remove(self.mot_obj[i-1])
    def M90(self):
        """w_e_o tonique > w_a_"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,e_o] and self.mot_obj[i-1][1] == w_: son[1] = a_
    def M94(self):
        """effacement du e_ en hiatus"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in isvoyelle and self.mot_obj[i-1][1] == e_ and i != 0:
                self.mot_obj.remove(self.mot_obj[i-1])
    def M98(self):
        """relachement de y_+l_ > y_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == l_ and self.mot_obj[i-1][1] == y_: self.mot_obj.remove(son)
    def M99(self):
        """monophtongaison de o_f/o_o + u_ > u_"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == u_ and self.mot_obj[i-1][1] in (o_o,o_f):
                self.mot_obj.remove(self.mot_obj[i-1])
    def M101(self):
        """simplification dipht coalescence a_u_ tonique > o_f"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,u_] and self.mot_obj[i-1][1] == a_:
                self.mot_obj[i-1:i+1] = [[3,o_f]]
    def M103(self):
        """nasalisation du e_o > a_n"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (n_,m_) and self.mot_obj[i-1][1] == e_o:
                self.mot_obj[i-1][1] = a_n
    def M104(self):
        """loi de position: e_f s'ouvre en e_o devant consonne articulé (dental/alvéolaire)"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (n_,t_,d_,s_,z_,r_,r_v,l_,ch_,j_) and self.mot_obj[i-1][1] == e_f and self.mot_obj[0][1] != e_o:
                self.mot_obj[i-1][1] = e_o
    def M105(self):
        """seconde nasalisation: suppression du n_/m_ et dénasalisation partielle : si nasale de vant voyelle:
            a_n > a_, e_n > e_, o_n >o sinon suppression nasale"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (n_,m_) and self.mot_obj[i-1][1] == a_n and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isvoyelle:
                self.mot_obj[i-1][1] = a_
            if son[1] in (n_,m_) and self.mot_obj[i-1][1] == e_n and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isvoyelle:
                self.mot_obj[i-1][1] = e_f
            if son[1] in (n_,m_) and self.mot_obj[i-1][1] == o_n and i != len(self.mot_obj)-1 and self.mot_obj[i+1][1] in isvoyelle:
                self.mot_obj[i-1][1] = o_o
            elif son[1] in (n_,m_) and self.mot_obj[i-1][1] in (a_n,e_n,o_n):
                self.mot_obj.remove(son)
    def M106(self):
        """assimilation de m_+n_ > m_ et ouverture de la voyelle précédente e_f > e_o"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == n_ and self.mot_obj[i-1][1] == m_:
                self.mot_obj.remove(son)
                if self.mot_obj[i-2][1] == e_f: self.mot_obj[i-2][1] = e_o
    def M107(self):
        """nasalisation de o_f/o_o"""
        for i,son in enumerate(self.mot_obj):
            if son[1] in (n_,m_) and self.mot_obj[i-1][1] in (o_f,o_o):
                self.mot_obj[i-1][1] = o_n
    def M109(self):
       """loi de position: o_f s'ouvre en o_o devant consonne articulé (dental/alvéolaire)"""
       for i,son in enumerate(self.mot_obj):
           if son[1] in (n_,t_,d_,s_,z_,r_,r_v,l_,ch_,j_) and self.mot_obj[i-1][1] == o_f and self.mot_obj[0][1] != o_o:
               self.mot_obj[i-1][1] = o_o
    def M110(self):
        """relachement diphtongue: e_o/e_f + w_ tonique > o_f"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,w_] and self.mot_obj[i-1][1] in (e_o,e_f):
                self.mot_obj[i-1:i+1] = [[3,o_f]]
            elif son[1] == w_ and self.mot_obj[i-1][1] in (e_o,e_f) and len(self.mot_obj) < 4:
                self.mot_obj[i-1:i+1] = [[3,o_f]]
    def M111(self):
        """u_o_f > u_e_f """
        for i,son in enumerate(self.mot_obj):
            if son == [3,o_f] and self.mot_obj[i-1][1] == u_:
                son[1] = e_f
    def M114(self):
        """u_e_f > u_eu_ """
        for i,son in enumerate(self.mot_obj):
            if son == [3,e_f] and self.mot_obj[i-1][1] == u_:
                son[1] = eu_
    def M115(self):
        """u_eu_ > eu_"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,eu_] and self.mot_obj[i-1][1] == u_:
                self.mot_obj.remove(self.mot_obj[i-1])
    def M116(self):
        """loi de position : ouverture de eu_ > oe_ en finale devant consonne articulée"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,eu_] and len(self.mot_obj) > 2 and i!= len(self.mot_obj) and self.mot_obj[i+1][1] in (f_,v_,n_,t_,d_,s_,z_,l_,r_):
                son[1] = oe_
    def M112(self):
        """réduction des diphtongues latines : a_e_f latin > e_o, o_f e_f > e_f"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == e_f and self.mot_obj[i-1][1] == a_:
                self.mot_obj[i-1:i+1] = [[son[0],e_o]]
    def M113(self):
        """réduction des diphtongues latines : a_e_f latin > e_o, o_f e_f > e_f"""
        for i,son in enumerate(self.mot_obj):
            if son == [3,e_f] and self.mot_obj[i-1] == [3,o_f]:
                self.mot_obj[i-1:i+1] = [[son[0],e_f]]
    def M117(self):
        """correction de l'amuïsement de g_v en final"""
        if self.mot_obj[-1][1] == g_f:self.mot_obj.remove(self.mot_obj[-1])
    def M118(self):
        """fixation de c_u_ (anci qu) + a > c_ + a_ en initial"""
        if len(self.mot_obj) > 2 and self.mot_obj[2][1] == a_ and self.mot_obj[1][1] == u_ and self.mot_obj[0][1] == c_:
            self.mot_obj.remove(self.mot_obj[1])
    def M119(self):
        """nasalisation de i_ derrière n_ > e_n"""
        for i,son in enumerate(self.mot_obj):
            if son[1] == n_ and self.mot_obj[i-1][1] == i_ and (i== len(self.mot_obj)-1 or self.mot_obj[i+1][1] in isconsonne):
                self.mot_obj[i-1:i] = [[son[0],e_n]]
                self.mot_obj.remove(son)
            #((régler le problème de la diphtongue de coalescence o_o/o_f u_ > u_ (!= diphtongue fr o_f u_ > e_f u_)))
    #tentative de reconstruction de l'orthographe
    #["a","è","œ",'ẽ','œ̃',"e","é","ë","i","ü","u","o","ô",'õ','â','ã']
    def orth(self):
        mot_orth = self.mot_ch
        for i,car in enumerate(mot_orth):
            if car == "a" and mot_orth[i-1] == "w": mot_orth=mot_orth[:i-1]+"oi"+mot_orth[i+1:]
            if car == "ã": mot_orth=mot_orth[:i]+"an"+mot_orth[i+1:]
            if car in ("é","è") and i<len(mot_orth)-2 and mot_orth[i+1] in isconsonne_ch and mot_orth[i+2] in isconsonne_ch:
                mot_orth=mot_orth[:i]+"e"+mot_orth[i+1:]
            if car == "õ": mot_orth=mot_orth[:i]+"on"+mot_orth[i+1:]
            if car == "ẽ": mot_orth=mot_orth[:i]+"in"+mot_orth[i+1:]
            if car == "u": mot_orth=mot_orth[:i]+"ou"+mot_orth[i+1:]
            if car == "ô": mot_orth=mot_orth[:i]+"o"+mot_orth[i+1:]
            if car == "y" and mot_orth[i-1] in isvoyelle_ch:
                mot_orth=mot_orth[:i]+"ille"+mot_orth[i+1:]
            if car == "y" and mot_orth[i-1] in isconsonne_ch and mot_orth[i+1] in isvoyelle_ch:
                mot_orth=mot_orth[:i]+"i"+mot_orth[i+1:]
            if car == "ë": mot_orth = mot_orth[:i]+"eu"+mot_orth[i+1:]
            if car == "œ": mot_orth = mot_orth[:i]+"œu"+mot_orth[i+1:]
            if car in "é,è,e,i" and mot_orth[i-1] == "c": mot_orth = mot_orth[:i-1]+"qu"+mot_orth[i:]
        return mot_orth

    #méthodes d'exécution
    def exe_mut(self):
        self.M108()
        self.M1()
        self.M2()
        self.M28()
        self.M3()
        self.M112()
        self.M4()
        self.M5()
        self.M6()
        self.M7()
        self.M9()
        self.M27()
        self.M20()
        self.M10()
        self.M11()
        self.M12()
        self.M13()
        self.M35()
        self.M35_bis()
        self.M30()
        self.M31()
        self.M14()
        self.M41()
        self.M15()
        self.M19()
        self.M16()
        self.M17()
        self.M29()
        self.M8()
        self.M18()
        self.M21()
        self.M22()
        self.M23()
        self.M24()
        self.M25()
        self.M32()
        self.M34()
        self.M33()
        self.M36()
        self.M37()
        self.M51()
        self.M38()
        self.M39()
        self.M40()
        self.M118()
        self.M42()
        self.M43()
        self.M44()
        self.M45()
        self.M46()
        self.M47()
        self.M47_bis()
        self.M113()
        self.M111()
        self.M52()
        self.M26()
        self.M48()
        self.M50()
        self.M53()
        self.M54()
        self.M55()
        self.M60()
        self.M56()
        self.M66()
        self.M59()
        self.M58()
        self.M61()
        self.M62()
        self.M63()
        self.M78()
        self.M79()
        self.M110()
        self.M107()
        self.M80()
        self.M67()
        self.M68()
        self.M69()
        self.M106()
        self.M70()
        self.M103()
        self.M72()
        self.M57()
        self.M57_bis()
        self.M57_tris()
        self.M114()
        self.M71()
        self.M73()
        self.M74()
        self.M77()
        self.M78()
        self.M115()
        self.M64_bis()
        self.M64()
        self.M81()
        self.M82()
        self.M83()
        self.M84()
        self.M91()
        self.M85()
        self.M86()
        self.M87()
        self.M75()
        self.M76()
        self.M88()
        self.M89()
        self.M90()
        self.M92()
        self.M94()
        self.M95()
        self.M96()
        self.M119()
        self.M105()
        self.M65()
        self.M116()
        self.M100()
        self.M101()
        self.M99()
        self.M98()
        self.M102()
        self.M93()
        self.M104()
        self.M109()
        self.M97()
        self.M117()
        self.mot_ch = self.transliterration()
        return self.mot_obj,self.mot_ch,self.orth()

#==========================================================================
#              FONCTIONS DE SECRETUM
#==========================================================================
car_speciaux ="?,'.123456789-0"
def rect_orth(mot:str)-> str:
    mot = mot.lower()
    for i,car in enumerate(mot):
        if car == "r": mot = mot[:i]+"R"+mot[i+1:]
        if car == "e": mot = mot[:i]+"é"+mot[i+1:]
        if car == "q": mot = mot[:i]+"c"+mot[i+1:]
    return mot

def evolution_phrase(texte:str)->str:
    """Cette fonction prend en entré une phrase en latin puis isole chaque mot puis les transforme
    selon l'orthographe réformé utilisé ici avant de les faire muter puis de renvoyé une phrase "traduite" (en réalité mutée)"""
    texte_mut = str()
    lst_phrase = texte.split()
    for i,mot in enumerate(lst_phrase):
        if mot not in car_speciaux:
            mot_obj = Mot(mot)
            texte_mut = texte_mut + mot_obj.exe_mut()[2] + " "
        if mot == "'":
            texte_mut = texte_mut + str(lst_phrase[i+1]) + " "
            lst_phrase.remove(lst_phrase[i+1])
        elif mot in car_speciaux: texte_mut += mot
    return texte_mut

q1 = "cuando ' Simone ' Weil ést nata ?"
q2 = "cumt béné ' Rocher ' Saint ' Michel computa dē levāRé dē gāmbas ?"
#q3 = "cumt béné aRtis constRuctyōnis distīncti ' Notre ' Dame ' du ' Puy  habēRé ?"
q4 = "cualis camīnus paRtit dē ' Notre ' Dame ' du ' Puy ?"
q5 = "uno mutto , cualis causa  ' vierge ' Puy cui ést millōcus ?"
q6 = "dē cualis aRmas fundūtu ést fāctutum ' Notre ' Dame ' de ' France ?"
q10 = "Omt trupat cuid dēntus āngulus ?"
#q7 = "cualis īndustRia ' Ponote om tRopat hincoRa dēintus Rūga ' Des ' Tables ?"

print(evolution_phrase(q10))


def mutation_mutine()->str:
    """prend en parametre rien et fait muter étape par étape le mot rentré l'utilisateur jusqu'à avoir son dérivé"""
    mot = input("Entrez le mot que vous voulez faire muter : ")
    rep_orth = input("Le mot utilise-t-il l'orthographe réglementaire phonologique de l'évoluteur ? ")
    rep_det = input("Voulez-vous l'évolution détaillé ? ")
    if rep_orth == "non":
        mot = rect_orth(mot)
        rep_orth = "oui"
    if rep_orth == "oui":
        if rep_det == "non":
            print(mot)
            mot = Mot(mot)
            mot = mot.exe_mut()
            return f"mot muté : {mot[1]} \northographe reconstruite : {mot[2]}"
        if rep_det == "oui":
            mot = Mot(mot)
            mot.M108()
            mot.M1()
            mot.M2()
            mot.M28()
            mot.M3()
            mot.M112()
            mot.M4()
            mot.M5()
            mot.M6()
            mot.M7()
            mot.M9()
            print(f"bas-latin : {mot}")
            mot.M27()
            mot.M20()
            mot.M10()
            mot.M11()
            mot.M12()
            mot.M13()
            mot.M35()
            mot.M35_bis()
            mot.M30()
            mot.M31()
            mot.M14()
            mot.M41()
            mot.M15()
            mot.M19()
            mot.M16()
            mot.M17()
            mot.M29()
            mot.M8()
            mot.M18()
            mot.M21()
            mot.M22()
            mot.M23()
            mot.M24()
            mot.M25()
            mot.M32()
            mot.M34()
            mot.M33()
            mot.M36()
            mot.M37()
            print(f"proto-gallo-roman : {mot}")
            mot.M51()
            mot.M38()
            mot.M39()
            mot.M40()
            mot.M118()
            mot.M42()
            mot.M43()
            mot.M44()
            mot.M45()
            mot.M46()
            mot.M47()
            mot.M47_bis()
            mot.M113()
            mot.M111()
            mot.M52()
            mot.M26()
            mot.M48()
            mot.M50()
            mot.M53()
            mot.M54()
            mot.M55()
            mot.M60()
            mot.M56()
            mot.M66()
            print(f"ancien français archaïque : {mot}")
            mot.M59()
            mot.M58()
            mot.M61()
            mot.M62()
            mot.M63()
            mot.M78()
            mot.M79()
            mot.M110()
            mot.M107()
            mot.M80()
            mot.M67()
            mot.M68()
            mot.M69()
            mot.M106()
            mot.M70()
            mot.M103()
            mot.M72()
            print(f"ancien français : {mot}")
            mot.M57()
            print(mot.transliterration())
            mot.M57_bis()
            mot.M57_tris()
            mot.M114()
            mot.M71()
            mot.M73()
            mot.M74()
            mot.M77()
            mot.M78()
            mot.M115()
            print(f"ancien français tardif : {mot}")
            mot.M64_bis()
            mot.M64()
            mot.M81()
            mot.M82()
            mot.M83()
            mot.M84()
            mot.M91()
            mot.M85()
            mot.M86()
            mot.M87()
            mot.M75()
            mot.M76()
            mot.M88()
            print(f"moyen français : {mot}")
            mot.M89()
            mot.M90()
            mot.M92()
            mot.M94()
            mot.M95()
            mot.M96()
            mot.M119()
            mot.M105()
            mot.M65()
            mot.M116()
            mot.M100()
            mot.M101()
            mot.M99()
            mot.M98()
            print(f"français moderne : {mot}")
            mot.M102()
            mot.M93()
            mot.M104()
            mot.M109()
            mot.M97()
            mot.M117()
            mot.mot_ch = mot.transliterration()
            return f"mot muté : {mot.mot_ch} \northographe reconstruite : {mot.orth()}"
    return "rentrez une réponse valide (oui/non) ou une orthographe correcte"



#==========================================================================
#              TESTS
#==========================================================================
#print(mutation_mutine())

secretum_0 =[[1,(3,3,0)],[1,(1,2,0,0,0)],
             [3,(6,2,0)],[3,(3,5,1)],[3,(1,2,0,1,0)],
             [5,(3,2,0)],[5,(3,1,1,0,0)],[5,(1,1,1)]]

secretum_1 = [[1,s_],[1,e_f],
              [3,c_],[3,r_],[3,e_l],
              [5,t_],[5,u_],[5,m_]]

secretum_4 = Mot("sécRētum")
mensem = Mot("mēnsém")
spina = Mot("spīna")
wita = Mot("wīta")
sperem = Mot("spēRém")#espoir ne vient pas de sperem ! mais du déverbal de sperare... (il faut donc changer la scansion de sperem pour s'adapter à ce fait qu'il est un dérivé du XIIe)
debere = Mot("débēRé")
gubernaculum = Mot("gubéRnāculum")
regem = Mot("Rēgém")
arborem = Mot("āRboRém")
patrem = Mot("pātRém")
capra = Mot("cāpRa")
pedem = Mot("pédém")
rosa = Mot("Rosa")
ornamentum = Mot("oRnāméntum")
armatura = Mot("aRmatūRa")
causa = Mot("causa")
canem = Mot("caném")
mica = Mot("mīca")
asinus = Mot("asinus")
carum = Mot("carum")
castellum = Mot("castéllum")
dubitum = Mot("dubitum")
follis = Mot("follis")
aqua = Mot("acua")
cattus = Mot("cattus")
adsatis = Mot("adsatis")#forme en latin populaire de 'ad satis' (latin classique avec 'satis' seul, forme de syntaxe impériale/populaire)
plaga = Mot("plaga")
placere = Mot("plasiRé")#latin populaire de la forme tardive placiRé du latin classique placēRé
multum = Mot("multum")
caballus = Mot("cabāllus")
insula = Mot("īnsula")
maturum = Mot("matūRum")
calwum = Mot("calwum")
sperare = Mot("spēRāRé")
gubernare = Mot("gubéRnāRé")
femina = Mot("fēmina")
debere = Mot("débēRé")
spiritum = Mot("spiRītum")
murus = Mot("mūRus")
comitem = Mot("comitém")
hominem = Mot("hominem")
soror = Mot("soRoR")
focus = Mot("focus")
strictum = Mot("stRictum")
florem = Mot("floRém")
habere = Mot("habēRé")


