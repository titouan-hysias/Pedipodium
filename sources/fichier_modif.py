# -*- coding: utf-8 -*-
import os
"""Ne pas lancer sur pycharm sinon des [NUL] apparaitront et seront invisible sur python"""
def aide()->None: print("Les deux clases avec leurs methodes:\n\n    -Recherche() #Permet d'effectuer une recherche 'prefixe' de fichiers et de dossiers\n        -self.chercher_chemin(nom_fichier) #'Renvoie le chemin pour acceder au fichier dont le nom est passe en parametre\n        -self.lancer(nom_fichier) #Lance le fichier dont le nom est pasee en parametre si celui-ci est un fichier python\n\n    -Modification(nom_fichier) #Permet de mofifier un fichier dont le nom est passe en parametre\n        -self.lire() #Affiche le contenue du fichier avec le numero de la ligne et le curseur\n        -self.ecrire_debut(text) #Ecrit au debut du fichier le text donne en parametre\n        -self.ecrire(text) #Ecrit le text passe en parametre a la position du curseur\n        -self.ecrire_fin(text) #Ecrit a la fin du fichier le text donne en parametre\n        -self.droite() #Deplace le curseur d'un cran vers la droite\n        -self.gauche() #Deplace le curseur d'un cran vers la gauche\n        -self.clear() #Suprime le contenu du fichier\n        -self.supprimer #Supprime le fichier        -self.supprimer_precedent() #Supprime l'element precedent dans le fichier        -self.a_la_ligne() #Ecrit un entree")

class Recherche():

    def __init__(self)->None:
        self.chemin, self.chemin_base = os.path.join(os.getcwd(), "fichier_modif.py"), os.path.join(os.getcwd(), "fichier_modif.py")
        while self.chemin_base != os.path.dirname(self.chemin_base): self.chemin_base = os.path.dirname(self.chemin_base)
        self.chemin_base = self.chemin_base[:-1]

    def chercher_chemin(self, nom_fichier : str)->list or str:
        """Effectue un parcour prefixe d'un arbre."""
        """Attention :
            -L'utilisateur doit rentrer le nom exacte avec le .XXX"""
        global chemins
        chemins = []
        self.chercher_chemin2(nom_fichier, self.chemin_base)
        if chemins != [] : return chemins
        return f"Le fichier/dossier '{nom_fichier}' est introuvable."
    
    def chercher_chemin2(self, nom_fichier : str, chemin : str)->None:
        global chemins
        fichiers, dossiers = os.listdir(chemin), []
        for fichier in fichiers :
            c = os.path.join(chemin, fichier)
            if fichier == nom_fichier : chemins.append(c)
            if os.path.isdir(c) : dossiers.append(fichier)
        for dossier in dossiers : self.chercher_chemin2(nom_fichier, os.path.join(chemin, dossier))
            
    def lancer(self, nom_fichier : str)->None:
        """runfile ne marche pas sur pycharm, marche que sur python"""
        if ".py" in nom_fichier and self.chercher_chemin(nom_fichier) != f"Le fichier '{nom_fichier}' est introuvable.":
            runfile(self.chercher_chemin(nom_fichier)[0])
        

class Modification():
    
    def __init__(self, nom_fichier : str)->None:
        F = Recherche()
        self.nom_fichier = nom_fichier
        self.chemin = F.chercher_chemin(nom_fichier)[0]
        if os.path.isdir(self.chemin) : raise ValueError("Le document est un dossier")
        self.position = 0
        """Faire attention au \n caches au la fin des lignes"""
    
    def lire(self)->None:
        """N'affiche pas le curseur si a la toute fin du fichier"""
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(0)
        text, curseur, ligne = fichier.read().split("\n"), 0, ""
        for num_ligne in range(len(text)):
            if len(text[num_ligne]) + curseur < self.position or ligne != "" : curseur += len(text[num_ligne])
            else :
                for loop in range(len(text[num_ligne])):
                    if curseur + loop == self.position: ligne += "->"
                    ligne += text[num_ligne][loop]
                if "->" not in ligne: ligne += "->"
                text[num_ligne] = ligne
            print(f"{num_ligne + 1}:",text[num_ligne])
            curseur += 1
        fichier.close()

    def ecrire_debut(self, ligne : str)->None:
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(0)
        text = fichier.read()
        fichier.seek(0)
        fichier.write(ligne + text)
        self.position = len(ligne)
        fichier.close()
    def ecrire(self, ligne : str)->None:
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(0)
        nb_n = len(fichier.read(self.position).split("\n")) - 1
        fichier.seek(self.position + nb_n)
        text = fichier.read(-1)
        fichier.seek(self.position + nb_n)
        fichier.write(ligne + text)
        self.position += len(ligne)
        fichier.close()
    def ecrire_fin(self, ligne : str)->None:
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(0, 2)
        fichier.write(ligne)
        fichier.seek(0)
        self.position = len(fichier.read(-1))
        fichier.close()

    def ligne_precedente(self)->None:
        fichier = open(self.chemin,"r+", encoding="utf8")
        ligne = 2
        while ligne != 0 and self.position != 0:
            self.a_gauche()
            fichier.seek(0)
            nb_n = len(fichier.read(self.position).split("\n")) - 1
            fichier.seek(self.position + nb_n)
            if fichier.read(1) == "\n": ligne -= 1
        if self.position != 0: self.position += 1
        fichier.close()
    def a_droite(self)->None:
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(0)
        nb_n = len(fichier.read().split("\n")) - 1
        fichier.seek(0)
        if self.position < len(fichier.read()) - nb_n:
            self.position += 1
        fichier.close()
    def a_gauche(self)->None:
        if self.position > 0: self.position -= 1

    def clear(self)->None:
        """Ne marche pas sur pycharm a cause de [NUL] """
        self.supprimer_fichier()
        with open(self.chemin,"r+", "a+") as fichier: pass

    
    def supprimer_fichier(self)->None: os.remove(self.chemin)
        
    def supprimer_precedent(self)->None:
        """Marche pas sur pycharme"""
        self.position -= 1
        self.suprimer(1)

    def a_la_ligne(self)->None: self.ecrire("\n")

    def trouver_ligne_pseudo(self, nom : str)->int:
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(0)
        text = fichier.read().split("\n")   
        for num_ligne in range(len(text)):
            if "joueur['" + nom + "']" in text[num_ligne]:
                fichier.close()
                return num_ligne
        fichier.close()
        return -1
    
    
    def supprimer(self, num : int)->None:
        """Marque des [NUL] sur pycharme"""
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(0)
        debut = fichier.read(self.position)
        fichier.seek(self.position + num) 
        fin = fichier.read(-1)
        fichier.close()
        self.clear()
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.write(debut + fin)
        fichier.close()

    def supprimer_ligne(self)->None:
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(self.position)
        longueur = len(fichier.readline())
        fichier.close()
        self.supprimer(longueur) # +1
        
    def aller_ligne(self, num : int)->None: #probleme : curseur en fin de ligne precedente au lieu de debut ligne
        for _ in range(num - 1):
            self.ligne_suivante()
        
    def ligne_suivante(self)->None:
        fichier = open(self.chemin,"r+", encoding="utf8")
        fichier.seek(0)
        nb_n = len(fichier.read(self.position).split("\n")) - 1
        if nb_n < 0: nb_n = 0
        fichier.seek(self.position + nb_n)
        self.position += len(fichier.readline()) + 1
        fichier.close()
"""
M = Modification("Sauvegarde.py")
fichier = open(M.chemin,"r+", encoding="utf8")
fichier.write("joueur")"""